<?php

/**
 * class Logger qui écrit date;verbe;path;ip::port;agent dans le fichier access_date_actuelle.log pour chaque appel à index.php
 */
class Logger {
    /**
     * instance unique du Logger
     * @var Logger
     */
     private static $instance = null;

    /**
     * constructeur
     */
     private function __construct() {}

    /**
     * retourne l'instance du Logger
     * @return Logger
     */
     public static function GetInstance() : Logger
    {
      if (self::$instance == null) {
        self::$instance = new Logger();
      }
      return self::$instance;
    }

    /**
    * écrit date;verbe;path;ip::port;agent dans le fichier access_date_actuelle.log pour chaque appel à index.php
    * @return int retourn toujours 0 /!\ pas de gestion d'erreur
    */
    public function log() : int {
        $verbe = $_SERVER['REQUEST_METHOD'];
        $date = date('l jS \of F Y h:i:s A');
        $path = $_SERVER['REQUEST_URI'];
        $port = $_SERVER['REMOTE_PORT'];
        $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $agent = $_SERVER['HTTP_USER_AGENT'];
        $line = sprintf("'%s';'%s';'%s';'%s::%s';'%s'\n", $date, $verbe, $path, $ip, $port, $agent);
        file_put_contents( "access_date_actuelle.log", $line, FILE_APPEND);
        return 0;
    }
}
